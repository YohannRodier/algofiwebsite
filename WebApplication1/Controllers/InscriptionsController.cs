﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Model;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InscriptionsController : ControllerBase
    {
        private readonly WebApplication1Context _context;

        public InscriptionsController(WebApplication1Context context)
        {
            _context = context;
        }

        // GET: api/Inscriptions
        [HttpGet]
        public IEnumerable<Inscription> GetInscription()
        {
            return _context.Inscription;
        }

        // GET: api/Inscriptions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInscription([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var inscription = await _context.Inscription.FindAsync(id);

            if (inscription == null)
            {
                return NotFound();
            }

            return Ok(inscription);
        }

        // PUT: api/Inscriptions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInscription([FromRoute] int id, [FromBody] Inscription inscription)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inscription.Id)
            {
                return BadRequest();
            }

            _context.Entry(inscription).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InscriptionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Inscriptions
        [HttpPost]
        public async Task<IActionResult> PostInscription([FromBody] Inscription inscription)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Inscription.Add(inscription);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInscription", new { id = inscription.Id }, inscription);
        }

        // DELETE: api/Inscriptions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInscription([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var inscription = await _context.Inscription.FindAsync(id);
            if (inscription == null)
            {
                return NotFound();
            }

            _context.Inscription.Remove(inscription);
            await _context.SaveChangesAsync();

            return Ok(inscription);
        }

        private bool InscriptionExists(int id)
        {
            return _context.Inscription.Any(e => e.Id == id);
        }
    }
}