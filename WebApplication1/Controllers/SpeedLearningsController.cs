﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Model;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpeedLearningsController : ControllerBase
    {
        private readonly WebApplication1Context _context;

        public SpeedLearningsController(WebApplication1Context context)
        {
            _context = context;
        }

        // GET: api/SpeedLearnings
        [HttpGet]
        public IEnumerable<SpeedLearning> GetSpeedLearning()
        {
            return _context.SpeedLearning;
        }

        // GET: api/SpeedLearnings/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSpeedLearning([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var speedLearning = await _context.SpeedLearning.FindAsync(id);

            if (speedLearning == null)
            {
                return NotFound();
            }

            return Ok(speedLearning);
        }

        // PUT: api/SpeedLearnings/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSpeedLearning([FromRoute] int id, [FromBody] SpeedLearning speedLearning)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != speedLearning.Id)
            {
                return BadRequest();
            }

            _context.Entry(speedLearning).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SpeedLearningExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SpeedLearnings
        [HttpPost]
        public async Task<IActionResult> PostSpeedLearning([FromBody] SpeedLearning speedLearning)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SpeedLearning.Add(speedLearning);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSpeedLearning", new { id = speedLearning.Id }, speedLearning);
        }

        // DELETE: api/SpeedLearnings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSpeedLearning([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var speedLearning = await _context.SpeedLearning.FindAsync(id);
            if (speedLearning == null)
            {
                return NotFound();
            }

            _context.SpeedLearning.Remove(speedLearning);
            await _context.SaveChangesAsync();

            return Ok(speedLearning);
        }

        private bool SpeedLearningExists(int id)
        {
            return _context.SpeedLearning.Any(e => e.Id == id);
        }
    }
}