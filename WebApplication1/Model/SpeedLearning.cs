﻿using System;

namespace WebApplication1.Model
{
    public class SpeedLearning
    {
        public int Id { get; set; }
        public string Auteur { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Niveau { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
        public string YoutubeVideo { get; set; }
        public string DescriptionDetaillee { get; set; }

    }
}
