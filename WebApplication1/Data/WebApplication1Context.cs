﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Model;

namespace WebApplication1.Models
{
    public class WebApplication1Context : DbContext
    {
        public WebApplication1Context (DbContextOptions<WebApplication1Context> options)
            : base(options)
        {
        }

        public DbSet<WebApplication1.Model.YoutubeVideos> YoutubeVideos { get; set; }

        public DbSet<WebApplication1.Model.SpeedLearning> SpeedLearning { get; set; }

        public DbSet<WebApplication1.Model.Inscription> Inscription { get; set; }
    }
}
